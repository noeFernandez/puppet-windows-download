# Class: download
#
# This module manages download
#
# Parameters: none
#
# Actions:
#
# Requires: see Modulefile
#
# Sample Usage: download{ "a title" : source => "http://sourceurl.com/my/internet/file/path.extension", destination => "C:\\MY\\LOCAL\\PATH\\filename.extension"
#
define download($source, $destination) {
  
  exec{"Download ${source}":
    command => template('download/downloadFile.ps1'), 
    onlyif => template('download/checkIfFileExists.ps1'), 
    provider => powershell,
  }

}
