# Puppet download type #

This is a puppet download module for windows. It allows you to retrieve files from the internet. To use it, just type:

```puppet
download{"Title":
  source => "a url to a internet file",
  destination => "your local path"
}
```

Example:

```puppet
download{"Virtual box":
  source => "http://download.virtualbox.org/virtualbox/4.3.24/VirtualBox-4.3.24-98716-Win.exe",
  destination => "C:\\Windows\\Temp\\virtualbox.exe"
}
```




